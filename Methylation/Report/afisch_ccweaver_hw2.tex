\documentclass{article} % For LaTeX2e
\usepackage{cos424, times}
\usepackage{hyperref}
\usepackage{url}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{amsfonts}
\usepackage{lmodern}
\usepackage{amssymb}
\usepackage{multicol}
\usepackage{enumitem}
\usepackage{array}
\usepackage{wrapfig,lipsum,booktabs}
\usepackage{subfigure}
\usepackage{caption}


\bibliographystyle{plos2009}

\title{Predicting DNA Methylation}

\nipsfinalcopy

\author{
Adam Fisch\\
Department of Mechanical Engineering\\
Princeton University\\
\texttt{afisch@princeton.edu} \\
\And
Campbell Weaver \\
Department of Electrical Engineering \\
Princeton University\\
\texttt{ccweaver@princeton.edu} \\
}

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\begin{document}

\maketitle

\begin{abstract}

DNA methylation is a small epigenetic modification that can have macro downstream associations. In this project we investigate methods for imputing missing methylation values across a genome based on limited data. We build and compare five regressions using various approaches, and evaluate their performance with root mean squared error, median absolute error, $r^2$ values, and computational time. Imputation on the test sample was found to be similar and moderately successful across all attempted methods. Linear regression performed the best, obtaining RMSE and $r^2$ values up to 0.0595 and 0.876, respectively.

\end{abstract}

\section{Introduction}

Gene expression in individuals is not solely contingent on the underlying DNA sequence. Rather, genes are also affected by epigenetic changes. These changes can turn genes on or off, and modify their behavior over time or with lifestyle \cite{simmons}. A commonly researched form of epigenetic modification is DNA methylation: when a methyl group binds with a cytosine base followed by a guanine base (a CpG site). DNA methylation is believed to play a crucial role in downstream cellular processes \cite{phillips}. It is correlated with long term health effects such as aging and cancer \cite{engelhardt}.

Knowing the locations of methylated CpG sites is an area of great scientific interest. However, obtaining full DNA methylation data is difficult. Whole genome bisulfate sequencing (WGBS) is available and makes it possible to get a nucleotide-level mapping of methylated DNA, yet this process is prohibitively expensive.  A much cheaper alternative is the Infinium HumanMethylation450 BeadChip Kit, which only looks at a much smaller subset of CpG sites \cite{engelhardt}. Here we seek to use a small sampling of inexpensive Infinium chip data to reconstruct the rest of the genome methylation data by using machine learning prediction approaches. 

% Profiling genome wide methylation patterns can shed light on understanding the influences on larger scaler epigenetic changes \cite{laird}. 

\section{DNA Methylation Dataset}

The Ziller et all. \cite{ziller} WGBS dataset for chromosome 1 is used for training and testing. The dataset consists of 33 reference samples taken from different areas, where each genomic position has a computed $\beta$ value ($\beta \in [0,1]$) that corresponds to the methylation level at that site. Microarray probe data gives $\beta$ measurements for $\sim$2\% of the DNA sites for an additional held out sample that is to be imputed.

Corresponding locations between the reference WGBS data and the probed microarray sites are identified and extracted to create a training set of feature vectors $\textbf{X}_t$ and target values $\textbf{y}_t$. The remaining data is used for a validation set $\textbf{X}_v$ and ground truth values $\textbf{y}_v$. The reference sample data was preprocessed by replacing any corrupted or missing $\beta$ values (NaN's) with the mean of the 33 samples at that particular site.

To augment the relatively simple predictor space formed by just the reference sample values, we experimented with adding several features that captured structural information. We cross referenced site indices with known chr1 CpG island locations, and encoded this attribute as a binary value. CpG island information was downloaded from the UCSC Genome Browser Annotation database \cite{genome}. We also included a variable for the DNA strand (+/-) that the site was located on, as well as the $\beta$ value of the closest site by kilobases that was probed on the microarray.


\section{Prediction Methods}
	
\subsection{Algorithms}

	Regression-based machine learning methods are a popular and successful approach for modeling the relationships between input variables and output values. Existing algorithms are quite diverse, ranging, for example, from the most basic best-fit lines produced by univariate linear regressions to the highly nonlinear, memory-based hypotheses produced by the $k$-Nearest Neighbor method \cite{nlp}.
	
	One of the most important concepts with regards to prediction algorithms is the bias-variance tradeoff. Over-simplified models have high bias; they are consistently wrong for varied inputs. Conversely, over-fitted models that are very sensitive to their training data have high variance. Prediction functions based off of different example sets will be inconsistent \cite{MLAPP}. For DNA methylation imputation, we sample from a range of simplified and complex regression approaches and compare performance. All methods were built using the SciKit-Learn library \cite{scikit}. Project source code can be found at \cite{bitbucket}.

\begin{enumerate}[leftmargin=*]

\item \emph{Ordinary Least Squares Regression} (OLS): OLS is one of the most widely used regression techniques. The model assumes that the data is a linear function of the inputs $\textbf{x} = \{x_1,...,x_n\}$ with Gaussian noise, represented by $p(y|\textbf{x}, \mathbb{\theta}) = \mathcal{N}(y|\textbf{w}^T\textbf{x}, \sigma^2)$ \cite{MLAPP}. Maximizing the log-likelihood for this model is equivalent to minimizing the residual sum of squares: $RSS(\textbf{w}) \triangleq \sum_{i=i}^{N}(y_i - \textbf{w}^T\textbf{x}_i)^2$.

\item \emph{Ridge Regression}: Ridge regression is a penalized least squares regression. It results in smoother, more stable prediction curves and lower variance when presented with noisy data. This is done by suppressing large parameters weights in $\textbf{w}$ with a zero-mean normal prior \cite{MLAPP}: $p(\textbf{w}) = \Pi_j\mathcal{N}(w_j|0,\tau^2)$. The solution minimizes the empirical loss combined with $L_2$ regularization of the weights, $RSS + \lambda\sum_{i=1}^p|w_i|^2$ \cite{MLAPP, AIMA}. Using cross validation, we chose $\lambda = 0.5$.

\item \emph{Lasso Regression}: Lasso is a similar model complexity shrinkage method to Ridge Regression. Lasso, however, uses $L_1$ regularization on $\textbf{w}$ instead of $L_2$, and seeks to minimize $\frac{1}{2N}RSS + \lambda\sum_{i=1}^p|w_i|$. A $L_1$ penalty tends to produce sparse models where many weights are set to zero. Effectively, this discards features that are considered less relevant, and makes hypothesis that are easier to humanly understand \cite{AIMA}. Through cross validation, we found a good value of $\lambda$ to be 5.95e-05.

\item \emph{Random Forest Regression} (RF): Random forests are a form of ensemble learning, and make nonlinear predictions when used for regression. They consist of a collection of smaller decision trees trained on different bootstrapped samples from the training data. The output of the RF is the average of the prediction outcomes of the component trees. RF typically performs well, with both low-variance and low-bias \cite{dasgupta}. We use an ensemble of 10 trees, each with a maximum branch depth of 100.

\item \emph{$k$-Nearest Neighbors Regression} (KNN): Given an input feature vector, the KNN algorithm looks in the training set for the $K$ closest examples and returns the average. This approach can have good performance, especially with expansive datasets. However, the algorithm must have a good distance metric to compare feature vectors. In high dimensional space, KNN suffers from the ``curse of dimensionality'' \cite{MLAPP}, where the $K$ nearest neighbors can still be very dissimilar. We use $K = 5$.

\end{enumerate}

\subsection{Evaluation Metrics}

A frequently used metric for regression is the root mean squared error (RMSE). The RMSE gives a measure of the standard deviation of the predicted values ($\hat{y}$) from the ground truth ($y$). The RMSE is calculated as $\sqrt{\frac{1}{N}\sum_{i = 1}^{N}(y _i- \hat{y}_i})^2$.

In addition to the RMSE, we look at the median absolute error (MAE). This metric takes the median of all the absolute differences of predicted and true values, $|y_i - \hat{y_i}|$ $i \in \{1, N\}$. MAE is a useful metric as it is robust to outliers in the data.

Both RMSE and MAE are scale dependent metrics. The coefficient of determination ($r^2$) is independent of scale and is a classic measure of how correlated the true and predicted values are. Values are [0, 1] with higher values indicating better performance, and is calculated as $1 - \frac{RSS}{\sum_{i=1}^N(y_i - \bar{y})^2}$ where $\bar{y}$ is the mean of $\textbf{y}$. 
\section{Results and Discussion}
\label{results}

\subsection{Evaluation Results}

We found that the different prediction approaches all performed similarly on the chr1 dataset. Table \ref{tab:metrics} lists the metrics for the five regression models. Additionally, these results are compared to the naive imputation of taking the mean of the reference samples. Overall, all approaches had moderate success, with RMSE values below 0.08, which is $>$30\% below the RMSE of the naive mean. The regressions also showed decent correlation, with $r^2$ values up to 0.876. All three of the linear regressions had nearly identical values for RMSE, MAE, and $r^2$. Random Forest and $k$-Nearest Neighbors performed comparably, with slightly higher RMSE and MAE values and lower $r^2$ scores. However, the computational time of KNN was orders of magnitude greater than the other methods. The addition of the extra structural features did not show any significant difference.

\begin{table}[htbp]
\small
\setlength{\tabcolsep}{6pt}
\setlength\extrarowheight{3pt} 
\centering
  \begin{tabular}{@{}|c|c|c|c|c|c|c|c|c|@{}} % Column formatting, @{} suppresses leading/trailing space 
  \hline
    &  \multicolumn{4}{c|}{Sample $\beta$ Values Only} &
  \multicolumn{4}{c|}{Sample $\beta$ Values + Structural Features}  \\
  \cline{2-9}
   Regression Type & RMSE  & MAE & $r^2$ & Time (s) & RMSE  & MAE & $r^2$ & Time (s)  \\ \hline\hline
      Mean $\beta$ &  0.116 & 0.0678& 0.531 & 0.018& 0.116 & 0.0678 & 0.531 & 0.012 \\
      OLS   &  0.0597 & 0.0345 & 0.876 & 0.0345 & 0.0596& 0.0345& 0.876& 0.0328 \\
      Ridge   &  0.0596 & 0.0345  & 0.876 & 0.199 & 0.0596& 0.0345 & 0.876 & 0.184 \\
      Lasso   &  0.0595 & 0.0345 & 0.876 & 0.454 & 0.0595 & 0.0344 & 0.876 & 0.453 \\
      RF   &  0.0622 & 0.0355 & 0.865 & 2.85 & 0.0623 & 0.0353 & 0.864 & 3.56 \\
      KNN  &  0.0715 & 0.0387 & 0.821 & 143 & 0.0728 & 0.0393& 0.815 & 165 \\
  \hline
  \end{tabular}
  \caption{Results from five regressors on test data. Naive mean $\beta$ value imputation performance is provided as a benchmark reference.}
   \label{tab:metrics}
\end{table}

 
 \subsection{Residuals Analysis}

The optimization algorithm for OLS regression operates on the assumption that there will be normally distributed error in $y - \textbf{w}^T\textbf{x}$. From Fig.~\ref{fig:fig1} we see that this assumption was valid. Plot (b) shows that a histogram of the residuals is close to a zero-mean gaussian. Interestingly, while the overall residual distribution is gaussian, from (a) it does not appear to be quite uniform in terms of variance across the $\beta$ values. The smaller variance at both the lower and upper spectrum suggests that extreme predictions ($\beta << 1$ or $\beta \approx 1$) are slightly more precise.

In terms of ``learning," Fig.~\ref{fig:fig1} (c) shows the regularization effects of bigger data. As the size of the training set grows, gains in performance are seen through decreasing RMSE . This is particularly evident in the KNN method, which relies on example recall, and the non-penalized OLS regression which is subject to bias from outliers in sparse datasets.

\subsection{Feature Weightings}

The penalties in the Ridge and Lasso regressions serve as automatic feature selectors. Looking at resulting weight vectors provides important information as to which features are most relevant for this particular prediction. Fig.~\ref{fig:fig2} shows the weightings for each of the feature variables. Interestingly, many coefficients are set equal or close to 0, while only samples 19-22 have the majority of the weight. It might be that the unknown sample was selected from an area similar to those samples.

 \begin{figure}[t!]
  
  \centering
 \begin{subfigure}[]
 \centering
 \includegraphics[width = 4.50cm, height = 3.9cm]{residuals.png}
 \end{subfigure}
 \begin{subfigure}[]
 \centering
 \includegraphics[width = 4.50cm, height = 3.9cm]{distribution.png}
 \end{subfigure}
  \begin{subfigure}[]
 \centering
 \includegraphics[width = 4.50cm, height = 3.9cm]{figure_1.png}
 \end{subfigure}
  \caption{(a) - (b): Residuals distribution for OLS. The zero mean gaussian error assumption is supported, with a fitted normal distribution giving $\mu = 0.001$ and $\sigma^2 = 0.0036$. (c): Learning curve for increasing training set size.}
%mu = 0.00109425930218
%sigma = 0.0596346580888
\label{fig:fig1}
\end{figure}



% \begin{figure}[h!]
%  \label{fig:fig3}
% \includegraphics[width = 8 cm, height = 4.5 cm]{figure_1.png}
% \centering
%  \caption{Residuals distribution for OLS. Gaussian error is supported.}
%  
%\end{figure}



 \begin{figure}[h!]
 \includegraphics[width = 13 cm, height = 3.8cm]{weights.png}
 \centering
  \caption{Feature weighting given by Ridge (left) and Lasso (right) regressions.}
   \label{fig:fig2}
\end{figure}

\section{Conclusion}

In this report we developed and compared five machine learning approaches applied to imputing DNA methylation values. All of the predictors performed moderately well on our dataset, as evaluated by root mean squared error, median absolute error, $r^2$ value, and computational time. While metrics were similar across all tested algorithms, the linear regressions showed marginally superior performance --- both in terms of error and run-time.

While the training data obtained from the Infinium chip data surveys just a tiny subsample of the total DNA sequence, we showed that regression results began to converge to low RMSE values after training on only a few hundred sites. Additionally, $L_1$ and $L_2$ regularization identified only a smaller subset of samples as being important. These findings bode well for imputation success in situations in which reference data might be even more limited.

 Finally, while results were positive, they are still not accurate enough for reliable downstream use. Ideally, $r^2$ values should be $>$0.98 in order for imputation values to be used confidently. Moving forward, there are several directions that can be taken to improve results and increase sensitivity. Additional indicative features can be mined, such as DNA sequence patterns, histone modification marks, transcription factor binding sites, etc \cite{engelhardt}. More sophisticated statistical models such as Gaussian processes might also be useful.
\bibliography{ref}



\end{document}











