###############################################################################
# afisch_ccweaver_hw2.py
# Authors: Adam Fisch and Campbell Weaver
#
# COS 424 Assignment 2
###############################################################################
from sklearn import linear_model
from sklearn import ensemble
from sklearn import neighbors
from sklearn import metrics
from sklearn.datasets import load_boston
from scipy.stats import norm
from itertools import izip_longest
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import time
import sys
import math

# Easy global var to tell when to suppress output
plotCoefs = False

###############################################################################
# A dummy regression test to use as a baseline. DummyMean returns the mean
# of the beta values passed in as features.
# Note: Only beta values can be used as features.
###############################################################################
def dummyMean(Xtest):
    Xtest = Xtest[:,0:33]
    predictions = np.mean(Xtest, axis = 1)
    return predictions

###############################################################################
# Run a least squares linear regression. Optimizes the L2 norm.
###############################################################################
def ordinaryLeastSquares(Xtrain, ytrain, Xtest):
    ols = linear_model.LinearRegression()
    ols.fit(Xtrain, ytrain)
    predictions = ols.predict(Xtest)
    return predictions

###############################################################################
# Run a ridge linear regression. Penalized least squares.
###############################################################################
def ridgeRegression(Xtrain, ytrain, Xtest):
    ridge = linear_model.RidgeCV(alphas=[0.1, 0.5, 1.0, 2.0, 4.0, 8.0])
    ridge.fit(Xtrain, ytrain)
    predictions = ridge.predict(Xtest)
    print ("Ridge Regression finished. Alpha = %s" % ridge.alpha_)
    if plotCoefs:
        print ("Ridge Coefficients:")
        print ridge.coef_
        plt.figure()
        plt.subplot(1,2,1)
        plt.bar(range(len(ridge.coef_)),ridge.coef_ , width = 1)
        plt.xlabel("Feature Number")
        plt.ylabel("Weight")
        plt.title("Penalized Weights For Ridge Regression")
    return predictions

###############################################################################
# Run a Lasso linear regression. Sparse coefficient regression.
###############################################################################
def lassoRegression(Xtrain, ytrain, Xtest):
    lasso = linear_model.LassoCV()
    lasso.fit(Xtrain, ytrain)
    predictions = lasso.predict(Xtest)
    print ("Lasso Regression finished. Alpha = %s" % lasso.alpha_)
    if plotCoefs:
        print ("Lasso Coefficients:")
        print lasso.coef_
        plt.subplot(1,2,2)
        plt.bar(range(len(lasso.coef_)),lasso.coef_ , width = 1)
        plt.xlabel("Feature Number")
        plt.ylabel("Weight")
        plt.title("Penalized Weights For Lasso Regression")
    return predictions

###############################################################################
# Run a Random Forest Regression. Ensemble of decision trees.
###############################################################################
def RandomForest(Xtrain, ytrain, Xtest):
    randomF = ensemble.RandomForestRegressor(max_depth = 100)
    randomF.fit(Xtrain, ytrain)
    predictions = randomF.predict(Xtest)
    return predictions

###############################################################################
# Run a KNN Regression. Average of nearest neighbors.
###############################################################################
def KNN(Xtrain, ytrain, Xtest):
    knn = neighbors.KNeighborsRegressor()
    knn.fit(Xtrain, ytrain)
    predictions = knn.predict(Xtest)
    return predictions

###############################################################################
# Calculate the mean squared error, median absolute error, and r-squared
# coefficient for a set of truth values and predictions.
###############################################################################
def calcMetrics(predictions, truth):
    MSE = metrics.mean_squared_error(truth, predictions)
    RMSE = np.sqrt(MSE)
    absErrors = np.absolute(np.array(predictions) - np.array(truth))
    MAD = np.median(absErrors)
    r_2 = metrics.r2_score(truth, predictions)
    return (RMSE, MAD, r_2)

###############################################################################
# Train all regression types on a given training dataset and run predictions
# on a test set. Returns the names of the regressions run, the predictions
# made, the residuals, computational time, and perfomance metrics (MSE, MAD,
# r2_).
###############################################################################
def runRegressions(Xtrain, yTrain, Xtest, yTest):
    regression_type = []
    predictions = []
    residuals = []
    runTime = []
    metrics = []

    regression_type.append('Dummy Mean Regression')
    start = time.time()
    results = dummyMean(Xtest)
    runTime.append(time.time() - start)
    predictions.append(results)
    residuals.append(np.array(results) - yTest)
    metrics.append(calcMetrics(results, yTest))

    regression_type.append('Ordinary Least Squares')
    start = time.time()
    results = ordinaryLeastSquares(Xtrain, yTrain, Xtest)
    runTime.append(time.time() - start)
    predictions.append(results)
    residuals.append(np.array(results) - yTest)
    metrics.append(calcMetrics(results, yTest))

    regression_type.append('Ridge Regression')
    start = time.time()
    results = ridgeRegression(Xtrain, yTrain, Xtest)
    runTime.append(time.time() - start)
    predictions.append(results)
    residuals.append(np.array(results) - yTest)
    metrics.append(calcMetrics(results, yTest))

    regression_type.append('Lasso Regression')
    start = time.time()
    results = lassoRegression(Xtrain, yTrain, Xtest)
    runTime.append(time.time() - start)
    predictions.append(results)
    residuals.append(np.array(results) - yTest)
    metrics.append(np.array(calcMetrics(results, yTest)))

    regression_type.append('Random Forest Regression')
    start = time.time()
    results = RandomForest(Xtrain, yTrain, Xtest)
    runTime.append(time.time() - start)
    predictions.append(results)
    residuals.append(np.array(results) - yTest)
    metrics.append(np.array(calcMetrics(results, yTest)))


    regression_type.append('KNN Regression')
    start = time.time()
    results = KNN(Xtrain, yTrain, Xtest)
    runTime.append(time.time() - start)
    predictions.append(results)
    residuals.append(np.array(results) - yTest)
    metrics.append(np.array(calcMetrics(results, yTest)))

    predictions = np.array(predictions)
    residuals = np.array(residuals)
    runTime = np.array(runTime)
    metrics = np.array(metrics)

    return (regression_type, predictions, residuals, runTime, metrics)

###############################################################################
# Method to read in the dataset and preprocess it.
# Splits data into the training and testing sets, and imputes missing features.
# Takes arg from command line and adds features based on this
# none = just samples, samples_strand = samples and strand
# samples_strand_nearVals = all 3
###############################################################################
def readData(arg):
    train_raw = open('./intersected_final_chr1_cutoff_20_train_revised.bed')
    sample_raw = open('./intersected_final_chr1_cutoff_20_sample.bed')
    test_raw = open('./intersected_final_chr1_cutoff_20_test.bed')
    cpg_raw = open('./cpgIslandExt.txt')
    print ("OPENED FILES")

    Xtrain = []
    yTrain = []
    Xtest = []
    yTest = []

    cpg_island_bases = []
    train_bases = []
    test_bases = []

    # Split on tab, replace NaNs with average at location
    # Append to Xtrain array
    for train_row, sample_row, test_row, cpg_row in \
        izip_longest(train_raw, sample_raw, test_raw, cpg_raw, fillvalue=None):

        floats = []
        train_col = train_row.split('\t')
        sample_col = sample_row.split('\t')
        test_col = test_row.split('\t')
        if cpg_row and 'islands' in arg:
            cpg_col = cpg_row.split('\t')
            if cpg_col[1] == 'chr1':
                cpg_island_bases.append(float(cpg_col[2]))
                cpg_island_bases.append(float(cpg_col[3]))

        if np.isnan(float(test_col[4])):
            continue
        elif int(test_col[5]) == 0:
            yTest.append(float(test_col[4]))

        # Extract train data, separate out test data, impute
        for i in range(4, 37):
            floats.append(float(train_col[i]))
        x = np.array(floats)
        locus_med = np.median(x[~np.isnan(x)])
        x[np.isnan(x)] = locus_med

        # boolean strand data
        if 'strand' in arg:
            x = np.append(x, (train_col[3] == '+'))

        if int(train_col[-1]) == 1:
            Xtrain.append(x)
            train_bases.append(float(train_col[1]))
        else:
            Xtest.append(x)
            test_bases.append(float(train_col[1]))

        # yTrain from sample
        if int(sample_col[5]) == 1:
            yTrain.append(float(sample_col[4]))

    Xtrain = np.array(Xtrain)
    yTrain = np.array(yTrain)
    Xtest = np.array(Xtest)
    yTest = np.array(yTest)

    print ("Doing islands and near values...")

    # determining closest value for test (nearest from chip) and
    # train (nearest also on chip)
    if 'islands' in arg:
        prev_island_point = float("-inf")
        inIsland = False
        next_island_point = cpg_island_bases[0]
    if 'nearVals' in arg:
        test_close_vals = []
        train_close_vals = []
        test_cpg_island = []
        train_cpg_island = []

        train_close_vals.append(yTrain[1])
        prev_base = float("-inf")
        next_base = train_bases[0]
        i = 1 #index of sample bases locations
        x = 1 #index of cpg island locations

        for base in test_bases:
            if 'islands' in arg:
                # check to see if moving between another set island start/end
                while base > next_island_point:
                    inIsland = not inIsland
                    prev_island_point = next_island_point
                    if x < len(cpg_island_bases):
                        next_island_point = cpg_island_bases[x]
                        x = x + 1
                    else:
                        next_island_point = float("inf")
                        x = x + 1
                test_cpg_island.append(inIsland)

            # check to see if moving between another set of bases
            if base > next_base:
                #cpg island check for training
                if 'islands' in arg:
                    if next_base > prev_island_point \
                        and next_base < next_island_point:
                        train_cpg_island.append(inIsland)
                    else:
                        train_cpg_island.append(~inIsland)

                prev_base = next_base
                if i < 7522:
                    next_base = train_bases[i]
                    i = i + 1
                    #training values:
                    if next_base < np.average([prev_base, train_bases[i]]):
                        train_close_vals.append(yTrain[i-2])
                    else:
                        train_close_vals.append(yTrain[i])
                elif i == 7522:
                    next_base = train_bases[7522]
                    i = i + 1
                else:
                    next_base = float("inf")
                    i = i + 1
            if base < np.average([prev_base, next_base]):
                test_close_vals.append(yTrain[i-2])
            else:
                test_close_vals.append(yTrain[i-1])
        train_close_vals.append(yTrain[-2])
        train_close_vals = np.array([train_close_vals])
        test_close_vals = np.array([test_close_vals])

        train_cpg_island = np.array([train_cpg_island])
        test_cpg_island = np.array([test_cpg_island])

        Xtrain = np.concatenate((Xtrain, train_close_vals.T), axis=1)
        Xtest = np.concatenate((Xtest, test_close_vals.T), axis=1)

        if 'islands' in arg:
            Xtrain = np.concatenate((Xtrain, train_cpg_island.T), axis=1)
            Xtest = np.concatenate((Xtest, test_cpg_island.T), axis=1)

    return (Xtrain, yTrain, Xtest, yTest)

###############################################################################
# Helper function to return all multiples of two from 1 to n.
###############################################################################
def multiplesOf2(n):
    nums = []
    m = int(math.floor(math.log(n, 2)))
    for i in range(m):
        nums.append(math.pow(2, i))
    return nums

###############################################################################
# main for running the analysis
#
# Command line: python afisch_ccweaver_hw2.py <strand_nearVals_islands>
#
# Argument is optional.
###############################################################################
def main():
    print ("STARTING")
    ### Read in data and impute missing training values
    if len(sys.argv) > 1:
        (Xtrain, yTrain, Xtest, yTest) = readData(sys.argv[1])
    else:
        (Xtrain, yTrain, Xtest, yTest) = readData('samples')

    global plotCoefs
    plotCoefs = True
    ### run regression for full features
    (regression_type, predictions, residuals, run_time, metrics) = \
        runRegressions(Xtrain, yTrain, Xtest, yTest)

    ### print out metrics results
    print ('\nREGRESSION RESULTS')
    print ('\nRoot Mean Squared Errors...')
    print (metrics[:,0])
    print ('\nMedian Absolute Errors...')
    print (metrics[:,1])
    print ('\nR-Squared Values...')
    print (metrics[:,2])
    print ('\nRun Times...')
    print (run_time)

    ### plot residuals
    plt.figure()
    plt.scatter(predictions[1], residuals[1])
    plt.axhline(y = 0, linewidth = 2, linestyle = '--', color = 'k' )
    plt.xlabel('Beta Value')
    plt.ylabel('Residuals')
    plt.title('OLS Residuals')

    plt.figure()
    (n, bins, patches) = plt.hist(residuals[1], 50, normed = 1)
    mu = np.mean(residuals[1])
    sigma = np.std(residuals[1])
    print ("mu = %s" % mu)
    print ("sigma = %s" %sigma)
    y = mlab.normpdf(bins, mu, sigma)
    plt.plot(bins, y, 'k--', linewidth = 2)
    plt.xlabel('Residual')
    plt.ylabel('density')
    plt.title('Distribution of Residuals')

    ### learning curve for varying size of training set
    RMSEs = []
    intervals = multiplesOf2(Xtrain.shape[0])
    plotCoefs = False
    for i in intervals:
        print (i)
        Xtrain_limited = Xtrain[0:i+5,:]
        yTrain_limited = yTrain[0:i+5]
        (regression_type, predictions, residuals, run_time, metrics) = \
            runRegressions(Xtrain_limited, yTrain_limited, Xtest, yTest)
        RMSEs.append(metrics[:,0])

    plt.figure()
    RMSEs = np.array(RMSEs)
    for i in range(1,len(regression_type)):
        plt.plot(intervals, RMSEs[:,i], label=regression_type[i])
    plt.xlabel("Size of Training Set (num examples)")
    plt.ylabel("RMSE")
    plt.title("Prediction Learning Curves")
    plt.legend(loc = 'best', prop={'size':10})
    plt.show()

if __name__ == "__main__":
   main()
